# Smart AdServer - Display SDK

This archive contains the _Smart AdServer Display SDK_, some _samples_ to demonstrate how to integrate it into your app and some _third party SDK_ that you can use with the Smart AdServer mediation feature.

## Requirements

To use the _Smart AdServer Display SDK_, you need:

* A _Smart AdServer_ account (http://www.smartadserver.com).
* _Xcode 9_ or later.
* A device running _iOS 8_ or more.

## Samples

The SDK is shipped with several samples :

* _ObjCSample_: several standard integrations in Objective C
* _SwiftSample_: several standard integrations in Swift

## Changelog

The full changelog can be seen in the _CHANGELOG.md_ file or in the full documentation.

## Documentation

The full documentation of this SDK can be found here:
http://help.smartadserver.com
