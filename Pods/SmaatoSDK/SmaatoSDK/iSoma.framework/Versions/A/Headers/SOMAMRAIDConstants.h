//
//  SOMAMRAIDConstants.h
//  iSoma
//
//  Created by Henning Buck on 15.01.18.
//  Copyright © 2018 Smaato Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const kSOMAMRAIDJSContent;
extern NSString *const kSOMAMRAIDUrlScheme;
extern NSString *const kSOMAOrientationPropertyForceOrientationPortraitKey;
extern NSString *const kSOMAOrientationPropertyForceOrientationLandscapeKey;
extern NSString *const kSOMAOrientationPropertyForceOrientationNoneKey;

enum {
    SOMAMRAIDAdViewPlacementTypeInline,
    SOMAMRAIDAdViewPlacementTypeInterstitial
};
typedef NSUInteger SOMAMRAIDAdViewPlacementType;

enum {
    SOMAMRAIDAdViewStateHidden,
    SOMAMRAIDAdViewStateDefault,
    SOMAMRAIDAdViewStateExpanded,
    SOMAMRAIDAdViewStateResized
};
typedef NSUInteger SOMAMRAIDAdViewState;
