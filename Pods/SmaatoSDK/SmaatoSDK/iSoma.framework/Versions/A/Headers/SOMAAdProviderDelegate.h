//
//  SOMAAdProviderDelegate.h
//  iSoma
//
//  Created by Aman Shaikh on 23/06/14.
//  Copyright (c) 2014 Smaato Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SOMAAdObject.h"

@class SOMAAdProvider;

@protocol SOMAAdProviderDelegate <NSObject>
@required
-(void)adProvider:(SOMAAdProvider*)provider didProvideAd:(SOMAAdObject*)ad;
-(void)adProvider:(SOMAAdProvider*)provider didProvidedMediationResponse:(NSArray*)mediatedNetworks;
-(void)adProvider:(SOMAAdProvider*)provider didFailed:(NSError*)error;
@end
