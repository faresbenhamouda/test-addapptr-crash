//
//  SOMAAdRequestParser.h
//  iSoma
//
//  Created by Aman Shaikh on 23/06/14.
//  Copyright (c) 2014 Smaato Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SOMAApiAdResponse.h"

@class SOMAAdObject;

@protocol SOMAAdParser <NSObject>
-(SOMAAdObject*)parseAdWithResponse:(SOMAApiAdResponse*)adResponse parsingError:(NSError**)error;
@end
