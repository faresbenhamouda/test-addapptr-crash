//
//  SOMAExtension.h
//  iSoma
//
//  Created by Aman Shaikh on 04.07.17.
//  Copyright © 2017 Smaato Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SOMAExtension <NSObject>
@property NSString* script;
@property NSDictionary* conf;
@property NSString* name;
@end
