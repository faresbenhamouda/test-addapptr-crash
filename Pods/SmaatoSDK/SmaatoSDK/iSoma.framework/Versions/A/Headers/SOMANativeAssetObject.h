//
//  SOMANativeAssetObject.h
//  iSoma
//
//  Created by Denisia Enasescu on 23/07/2018.
//  Copyright © 2018 Smaato Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SOMANativeAssetObject : NSObject

/**
 Unique asset ID. Typically a counter for the array.
 */
@property(nonatomic, readonly) NSUInteger assetID;

/**
 Creates SOMANativeAssetObject with assetID property set
 
 @param assetID     Response from backend.
 @return            The initialized `SOMANativeAdObject` or `nil` on failure.
 */
- (id)initWithAssetID:(NSUInteger)assetID;

@end
