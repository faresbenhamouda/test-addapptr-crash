//
//  ViewController.swift
//  TestAddapptr
//
//  Created by Fares Ben Hamouda on 01.06.19.
//  Copyright © 2019 Fares Ben Hamouda. All rights reserved.
//

import UIKit

class ViewController: UIViewController, AATKitDelegate {

    var aatPlacement: AATKitPlacement?

    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var preloadButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        AATKit.setDelegate(self)
        aatPlacement = AATKit.createPlacement(withName: "InterstitialPlacement", andType: .fullscreen)
        startButton.isEnabled = false
    }
    
    @IBAction func openConsentClicked(_ sender: Any) {
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.managedConsent?.presentDialog(on: self, completionHandler: {
                delegate.reConfigureAddapptr()
            })
        }
    }
    
    @IBAction func preloadAd(_ sender: Any) {
        guard let pl = aatPlacement else {
            return
        }
        AATKit.setPlacementViewController(self, for: pl)
        AATKit.startPlacementAutoReload(pl)
    }
    
    @IBAction func startAd(_ sender: Any) {
        guard let pl = aatPlacement else {
            return
        }
        AATKit.show(pl)
    }
    
    func aatKitHaveAd(_ placement: AATKitPlacement) {
        preloadButton.isEnabled = false
        startButton.isEnabled = true
    }
    
    func aatKitShowingEmpty(_ placement: AATKitPlacement) {
        preloadButton.isEnabled = true
        startButton.isEnabled = false
    }
    
    func aatKitResumeAfterAd() {
        preloadButton.isEnabled = true
        startButton.isEnabled = false
    }
    
    
}

